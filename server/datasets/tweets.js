var mongoose = require('mongoose');
//MODEL FOR TWEETS
module.exports = mongoose.model('tweet', {

	user: String,
	userId: String,
	userImage: String,
	content: String,
	date: {type: Date, default: Date.now}
});