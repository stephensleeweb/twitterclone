/// <reference path = "angular.js" />
var mongoose = require('mongoose');
module.exports = mongoose.model('user', {
	email: String,
	username: String,
	password: String,
	image: String,
	bio: String,
	following: [{userId: String}],
	followers: [{userId: String}]
});