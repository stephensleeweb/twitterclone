var Users = require('../datasets/users');
module.exports.getUsers = function(req, res)
{
	Users.find({}, function(err, usersData){
		if(err)
		{
			res.error(err);
		}
		else {
			res.json(usersData);
			
		}
	})
}

module.exports.followUser = function(req, res)
{
	var userId = req.body.userId, personId = req.body.personId;
	console.log("this is person whom you can follow: ", personId, "This is YOU: ", userId);

	Users.findById(personId, function(err, person){
		person.followers.push({userId: userId});
		person.save();

	});

	Users.findById(userId, function(err, follower){
		follower.following.push({userId: personId });
		follower.save();

	})
}