var User = require('../datasets/users');

//use sudo npm install fs-extra : fs stands for file system which will help us move image 
//data around files.
/*
fs-extra is a drop in replacement for native fs. All methods in fs are unmodified and attached to fs-extra.

You don't ever need to include the original fs module again.
*/
//use sudo npm install path: This module contains utilities for handling and transforming 
//file paths. Almost all these methods perform only string transformations. 
//The file system is not consulted to check whether paths are valid.
var fs = require('fs-extra');
var path = require('path');


/******Module.exports is called "end-point"******/

module.exports.updatePhoto = function(req, res){
	var file = req.files.file;
	var userId = req.body.userId;

	console.log("User " + userId + " is submitting ", file);
	/*
		Set the file path where to store user image data into arranged by date
	*/
	var uploadDate = new Date();
	var tempPath = file.path;
	var targetPath = path.join(__dirname, "../../uploads/", userId + uploadDate + file.name);
	var savePath = "/uploads/" + userId + uploadDate + file.name;
	fs.rename(tempPath, targetPath, function(err){

		if(err){
			console.log(err);
		}
		else
		{
			//Storing the picture path is successful and store the image path into the user json object.
			//Find the userdata by userId!
			User.findById(userId, function(err, userData){
				var user = userData;
				user.image = savePath;
				user.save( function(err){
					if(err)
					{
						console.log("failed save");
						res.json({status: 500});
					} else
					{
						console.log("save succeeded");

						res.json({status: 200});
					}
				});
			})
		}
	});

}

module.exports.updateUsername = function(req, res){
	var userId = req.body.userId;
	var username = req.body.username;

	
	User.findById(userId, function(err, userData){
		var user = userData;
		user.username = username;
		user.save( function(err){
			if(err)
			{
				console.log("failed save");
				res.json({status: 500});
			} else
			{
				console.log("save succeeded");

				res.json({status: 200});
			}
		})

	});

	

}

module.exports.updateBio = function(req, res){
	var userId = req.body.userId;
	var userBio = req.body.userBio;

	
	User.findById(userId, function(err, userData){
		var user = userData;
		user.bio = userBio;
		user.save( function(err){
			if(err)
			{
				console.log("fail updatebio");
				res.json({status: 500});
			} else
			{
				console.log("success updatebio");

				res.json({status: 200});
			}
		});
	})//EOF User.findById
}//EOF updateBio

