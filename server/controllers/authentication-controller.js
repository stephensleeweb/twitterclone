
var User = require('../datasets/users');

module.exports.signup = function(req, res){
	console.log(req.body); //testing purpose
	var user = new User(req.body); //Transfer the data from webserver to json data format for mongoDB.
	user.save(); //Save the new json object as req.body corresponding to User data format.
	res.json(req.body); //Now execute the requested call as response. (its like returning the value at the end.)
};

//module.exports: keep the properties available to the public when this module is available to 
//the other files. ex)var auth = require('./server/controllers/authentication-controller'); inside server.js
//app.post('/api/user/signup', authenticationController.signup);
//This is called callback method
module.exports.login = function(req, res){
	console.log("Login");
	//Find the user corresponding to the req.body
	User.find(req.body, function(err, results){
		if(err)
		{
			console.log("Error Out");
		}
		//Check the results existance and 
		if(results && results.length === 1)
		{	
			//If the results is found (meaning if the user is logged in or authenticated)
			//send back the user data.
			var userData = results[0];
			res.json({
				email: req.body.email,
				_id: userData._id,
				username: userData.username,
				image: userData.image,
				following: userData.following,
				followers: userData.followers
			});
			/*Send back as the response to database to the request from Express.*/
			
		}
	})
};

