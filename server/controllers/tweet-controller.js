
var Tweet = require('../datasets/tweets');

module.exports.postTweet = function(req, res) {
	
	// var tweet = req.body.content;
	var tweet = new Tweet(req.body);
	tweet.save();

	//Find all the tweets {} means basically it does not have constraints.
	//And sort it out starting from the most recent one at the top.
	/*Currently, we are doing the sorting on the client side but doing it on the server side would be recommended.*/
	Tweet.find({}).sort({date: -1}).exec( function(err, allTweets){

		if(err)
		{
			res.error(error);
		}	
		else {
			console.log(allTweets);
			res.json(allTweets);
		}
	});
};

module.exports.getTweet = function(req, res){

	Tweet.find({})
		.sort({date: -1})
		.exec(function(err, allTweets){
			if(err)
			{
				res.error(err)
			} else
			{
				res.json(allTweets);
			}
		})
}