//Stephen Sungsoo Lee
/// <reference path = "angular.js" />

var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var multipart = require('connect-multiparty');


var multipartMiddleware = multipart();
var app = express();
var authenticationController = require('./server/controllers/authentication-controller');
var profileController = require('./server/controllers/profile-controller');
var tweetController = require('./server/controllers/tweet-controller');
var usersController = require('./server/controllers/users-controller');
//Connect to the database at the localhost
mongoose.connect('mongodb://localhost:27017/SNS');

app.use(multipartMiddleware);
app.use(bodyParser.json()); 
//use body parser to transport json data objects form web server to the server.
app.use('/app', express.static(__dirname + "/app")); //basically, using all the files that are inside app folder
/*To serve static files such as images, CSS files, and JavaScript files, 
use the express.static built-in middleware function in Express.
Pass the name of the directory that contains the static assets to the express.
static middleware function to start serving the files directly.*/
app.use('/node_modules', express.static(__dirname + "/node_modules"));
app.use('/uploads', express.static(__dirname + "/uploads"));
app.get('/', function(req, res){
	res.sendfile('./public/index.html');
});

//Authentication

/*This code basically post the data from the web server to database server.*/
app.post('/api/user/signup', authenticationController.signup);
//Navigation controller handles the login (because all activities are handled after user is logged in.)
app.post('/api/user/login', authenticationController.login);

//profile
//Send the json object from api path (middleware) via express to execute updatePhoto.
app.post('/api/profile/editPhoto', multipartMiddleware, profileController.updatePhoto);
app.post('/api/profile/updateUsername', profileController.updateUsername);
app.post('/api/profile/updateBio', profileController.updateBio);

//tweets
app.post('/api/tweet/post', tweetController.postTweet);
app.get('/api/tweet/get', tweetController.getTweet);


//USer
app.get('/api/users/get', usersController.getUsers);
app.post('/api/users/follow', usersController.followUser);
app.listen('3000', function() {
	console.log("This is working listen to local host 3000");
});



/**How to find the users who signedup?
mongod //run it to set up the server connection.
mongo //run it to have access to the database.

db.users.find() //inside "mongo" terminal open.

**/

/**inside mongo***/
/*

show dbs
use [db name]
db.[collection].find() //ex) db.users.find()
db.users.remove({email: "sl@gmail.com"})

*/





