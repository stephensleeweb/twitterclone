//signup-controller


(function(){
    angular.module('SNS')
.controller('SignupController', ['$scope', '$state', '$http', function($scope, $state, $http){
        
        $scope.createUser = function(){
            
            $http.post('/api/user/signup', $scope.newUser).success(function(response){
            // make sure you '/api' not just api. Errr! You are posting the user data into webserver.
            //in the server.js, it takes this http post call, and transpot this data to the database with
            // '/api/user/signup' path and authenticaiton-controller.js
            }).error(function(error){
                console.log(error);
            })
        }
    }]);
}());


//$scope is the application object (the owner of application variables and functions).