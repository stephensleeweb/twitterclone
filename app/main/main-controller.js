(function(){
	angular.module('SNS')
	.controller('MainController', ['$scope', '$http', '$interval', 
		function ($scope, $http, $interval){

			if(localStorage['User-Data'] !== undefined){
				$scope.user = JSON.parse(localStorage['User-Data']);
				console.log($scope.user);
			}

			$scope.sendTweet = function(event){

				//If the user presses enter key (13), this will trigger...
				if(event.which === 13){
					var request = {
						user: $scope.user.username || $scope.user.email,
						userId: $scope.user._id,
						userImage: $scope.user.image,
						content: $scope.newTweet
					}
					$http.post('api/tweet/post', request).success(function(response){
						console.log(response);
						$scope.tweets = response;
						 /*Set these tweets to the response so that in our view, we can make the ng-repeat table that 
						 shows all the tweets*/


					}).error(function(error){
						console.error(error);
					})
				}
			};//EOF SendTweet


			function getTweets (initial){
				//If tweets are same as initial ones and have not been added, then set the response to tweets scope. Otherwise,
				//Set it to incomingTweets
				$http.get('api/tweet/get').success(function(response){
					if(initial)
					{
						$scope.tweets = response;

					}
					else 
					{

						if(response.length > $scope.tweets.length)
						{ 
							$scope.incomingTweets = response; //Set incomingTweets as the reponse from the server
						}
					}
				})
			}; //EOF getTweets function.

			$interval(function(){
				console.log("this is working");
				getTweets(false); //this will set incomingTweets to the response from the server.
				if($scope.incomingTweets)
				{
					$scope.difference = $scope.incomingTweets.length - $scope.tweets.length;
				}
			}, 5000); //Every 5 seconds this function is triggered.

			$scope.setNewTweets = function () 
			{	//Once user triggers setNewTweets, then that will disable the incomingTweets link and update current tweets with the 
				//incoming tweets.
				$scope.tweets = angular.copy($scope.incomingTweets);
				//hide the incomingTweets
				$scope.incomingTweets = undefined;
			}

			getTweets(true);

		}])
}());