(function(){
	angular.module('SNS')
	.controller('EditProfileController', ['Upload', '$scope','$state','$http', /*Install ng-file-upload. npm install ng file upload*/
		function(Upload, $scope, $state, $http){

		$scope.user = JSON.parse(localStorage['User-Data']) || undefined;

		// After login, pass the user data into localStorage.
		$scope.$watch(function(){
			return $scope.file
		}, function (){
			$scope.upload($scope.file); 
		}); //EOF watch


		$scope.upload = function(file) {
		    if (file){
		        Upload.upload({
		            url: 'api/profile/editPhoto',
		            method: 'POST',
		            data: {userId: $scope.user._id},
		            file: file
		        }).progress(function(evt){
		            console.log("firing");
		        }).success(function(data){
		            
		        }).error(function(error){
		            console.log(error);
		        })
		    }
		};//EOF upload
                        
		$scope.updateUsername = function () {

			var request = {
				userId: $scope.user._id,
				username: $scope.user.username
			}
			$http.post('api/profile/updateUsername', request).success(function(){
				console.log("success");
			}).error(function(error){
				console.log(error);
			}) //EOF http.post

		}; //EOF updateUsername

		$scope.updateBio = function () {
			var request = {
				userId: $scope.user._id,
				userBio: $scope.user.bio
			}

			$http.post('api/profile/updateBio', request).success(function(){
				console.log("success");
			}).error(function(error){
				console.log(error);
			}) //EOF http.post
		};


			
	}]) //EOF controller
}() //EOF function
)
