(function(){
	angular.module('SNS')
	.controller('FollowController', ['$scope', '$http', function($scope, $http){
		
		$scope.user = JSON.parse(localStorage['User-Data']);
		console.log($scope.user);
		$http.get('api/users/get').then(function(response){
			$scope.users = response.data;
			console.log($scope.users);
		})//EOF get method

		$scope.follow = function(userId, personId) {
			request = {userId: userId,
					personId: personId};
			$http.post('api/users/follow', request).then(function(){
				console.log("following ", personId);
				
			})
		} //EOF Follow
		
		// $scope.unfollow = function(userId, personId) {
		// 	request = {
		// 		userId: userId,
		// 		personId: personId
		// 	};
		// 	$http.post('api/users/unfollow', request).then(function(){
		// 		console.log("un-following ", personId);
		// 	});
		// }


		$scope.checkIsFollowing = function(personId)
		{

			for(var i = 0, len = $scope.user.following.length; i < len; i++)
			{
				console.log(len);
				if($scope.user.following[i].userId === personId)
				{
					return true;
				}
				
			}
			return false;
		}
	}]);
}());