(function(){
	angular.module('SNS')
	.controller('NavigationController', ['$scope', '$http','$state', function($scope, $http, $state){
		
		if(localStorage['User-Data']){

			$scope.loggedIn = true;
		}
		else{
			$scope.loggedIn = false;
		}

		
		$scope.logUserIn = function() {
			/**api call. This http post will trigger app.post() in server.js**/
			$http.post('api/user/login', $scope.login).success(function(response){
				//Local storage is more secure, and large amounts of data can be stored locally,
				// without affecting website performance
				/***On Developer Console, check localStorage and localStorage.clear()***/
				localStorage.setItem('User-Data', JSON.stringify(response));
				//this will initially store the response from the user into the local server.
				//So later on, user does not have to access the application but rather go into 
				//local server and query the data.
				$scope.loggedIn = true;
			}).error(function(error){
				console.error(error);
			});

		};//EOF logUserIn

		$scope.logOut = function () {
			localStorage.clear();
			$scope.loggedIn = false;
		} //EOF logOut

	}]);

}());