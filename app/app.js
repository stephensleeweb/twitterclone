/// <reference path = "angular.js" />

(function(){
	angular.module('SNS',['ui.router', 'ngFileUpload'])
	.config(function($stateProvider,$urlRouterProvider){

		//If URLs don't mach these states, then go to '/'
		$urlRouterProvider.otherwise('/');

		$stateProvider
			.state('signUp', {
				//when the state 'signUp' is triggered from index.html, it goes to '/signup' with 
				//the template signup.html and its controller.
				url: "/signup",
				templateUrl: "app/signup/signup.html",
				controller: "SignupController"
			})
			.state('editProfile', {
				url: "/edit-profile",
				templateUrl: "app/profile/edit-profile-view.html",
				controller: "EditProfileController"
			})
			.state('Main', {
				url: "/",
				templateUrl: "app/main/main.html",
				controller: "MainController"
			})
			.state('follow',{
				url: "/follow-users",
				templateUrl: "app/follow/follow.html",
				controller: "FollowController"
			})
	}); 
}());



//module('name of applicaiton', dependencies)
//  think of a module as a container for the different parts of your app – controllers, services, filters, directives, etc
// stop different ports to running into each other.